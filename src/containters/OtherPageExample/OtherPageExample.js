import React, { Component } from 'react';

// class OtherPageExample extends Component {
//     render() {
//         return (
//             <div>
//                 <h3> Other Page</h3>
//             </div> 
//         );
//     }
// }

const OtherPageExample = () => (
    <div>
        <h3> Other Page</h3>
    </div> 
)

export default OtherPageExample;