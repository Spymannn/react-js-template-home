import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import SplitText from 'react-pose-text';
import './HomePage.css';

const charPoses = {
    exit: { y: 30, opacity: 0 },
    enter: {
      y: 0,
      opacity: 1,
      transition: ({ charInWordIndex }) => ({
        type: 'spring',
        delay: charInWordIndex * 30,
        stiffness: 500 + charInWordIndex * 150,
        damping: 10 - charInWordIndex * 1
      })
    }
  };

  
class HomePage extends Component {
    render() {
        return (
            <div className="header-content">
                <h1 className="header-title">Your personal yoga app</h1>
                <div class="wrapper">
                    <div class="focus">
                    YOGA
                    </div>
                    <div class="mask">
                        <div class="text">YOGA</div>
                    </div>
                    </div>
                <h3 className="header-subtitle">A useful start to feel great</h3>
                <Link to={`/otherPageExample`} className="Menu-list-item">
                    <SplitText initialPose="exit" pose="enter" charPoses={charPoses} className="link-split-text">
                        Enter
                    </SplitText>
                    <span class="Mask"><span>Enter</span></span>
                    <span class="Mask"><span>Enter</span></span>

                </Link>
            </div>
  
        );
    }
}

export default HomePage;
